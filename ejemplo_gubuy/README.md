CONTENIDOS DE ESTE ARCHIVO
---------------------

 * Introducción
 * Requerimientos
 * Instalación
 * Configuración
 * Observacion.

 INTRODUCCIÓN
------------

 Este módulo crea una entidad personalizada y una vista que lista
 elementos de dicha entidad segun lo descrito en el ejercicio propuesto.

 REQUERIMIENTOS
------------

No tiene requerimientos, solo el core de drupal.

INSTALLACIÓN
------------

 Se puede instalar con drush (drush en ejemplo_gubuy) o a través
 de la interfaz de drupal. 

CONFIGURACIÓN.
-------------

El modulo no requiere configuración adicional.

OBSERVACIÓN.
-------------

El ordenamiento de la vista se resolvio usando un hook_views_pre_render
debido al tiempo, quedaría pendiente encontrar una mejor solución para el
ordenamiento.
