<?php

/**
 * @file
 * Ejemplo Gubuy module functionality.
 */

/**
 * Implements hook_views_data_alter().
 */
function ejemplo_gubuy_views_data_alter(array &$data) {
  $data['views']['table']['group'] = t('Resultado');
  $data['views']['table']['join'] = [
      // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];
  $data['views']['ejemplo_gubuy_resultado'] = [
    'title' => t('Resultado'),
    'help' => t('Calcula valores usando dato 1 y dato 2 para Ejemplo Gubuy Entity.'),
    'click sortable' => TRUE,
    'field' => [
      'id' => 'ejemplo_gubuy_resultado',
    ],
  ];
  return $data;
}
