<?php

namespace Drupal\ejemplo_gubuy;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the ejemplo_gubuy entity.
 *
 * @see \Drupal\ejemplo_gubuy\Entity\EjemploGubuyEntity.
 */
class EjemploGubuyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ejemplo_gubuy\Entity\EjemploGubuyEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ejemplo_gubuy entity entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published ejemplo_gubuy entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit ejemplo_gubuy entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete ejemplo_gubuy entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ejemplo_gubuy entity entities');
  }

}
