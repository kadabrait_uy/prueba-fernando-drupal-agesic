<?php

namespace Drupal\ejemplo_gubuy;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of ejemplo_gubuy entities.
 *
 * @ingroup ejemplo_gubuy
 */
class EjemploGubuyListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['dato_1'] = $this->t('Dato 1');
    $header['dato_2'] = $this->t('Dato 2');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\ejemplo_gubuy\Entity\EjemploGubuyEntity $entity */
    $row['id'] = $entity->id();
    $row['dato_1'] = $entity->dato_1->value;
    $row['dato_2'] = $entity->dato_2->value;
    return $row + parent::buildRow($entity);
  }

}
