<?php

namespace Drupal\ejemplo_gubuy\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;

/**
 * Defines the EjemploGubEntity entity.
 *
 * @ingroup ejemplo_gubuy
 *
 * @ContentEntityType(
 *   id = "ejemplo_gubuy",
 *   label = @Translation("Ejemplo Gubuy Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ejemplo_gubuy\EjemploGubuyListBuilder",
 *     "views_data" = "Drupal\ejemplo_gubuy\Entity\EjemploGubuyViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ejemplo_gubuy\Form\EjemploGubuyForm",
 *       "add" = "Drupal\ejemplo_gubuy\Form\EjemploGubuyForm",
 *       "edit" = "Drupal\ejemplo_gubuy\Form\EjemploGubuyForm",
 *       "delete" = "Drupal\ejemplo_gubuy\Form\EjemploGubuyDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ejemplo_gubuy\EjemploGubuyHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ejemplo_gubuy\EjemploGubuyAccessControlHandler",
 *     },
 *   base_table = "ejemplo_gubuy",
 *   translatable = FALSE,
 *   admin_permission = "administer ejemplo_gubuy entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "dato_1" = "dato_1",
 *     "dato_1" = "dato_2",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   constraints = {
 *     "EjemploGubuyUniqueEntity" = {}
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ejemplo_gubuy/{ejemplo_gubuy}",
 *     "add-form" = "/admin/structure/ejemplo_gubuy/add",
 *     "edit-form" = "/admin/structure/ejemplo_gubuy/{ejemplo_gubuy}/edit",
 *     "delete-form" = "/admin/structure/ejemplo_gubuy/{ejemplo_gubuy}/delete",
 *     "collection" = "/admin/structure/ejemplo_gubuy/list",
 *   },
 *   field_ui_base_route = "ejemplo_gubuy.settings",
 * )
 */
class EjemploGubuyEntity extends ContentEntityBase implements EjemploGubuyEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getDato1() {
    return $this->get('dato_1')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDato1($dato) {
    return $this->set('dato_1', $dato);
  }

  /**
   * {@inheritdoc}
   */
  public function getDato2() {
    return $this->get('dato_2')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDato2($dato) {
    return $this->set('dato_2', $dato);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('Identificador de la entidad.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['dato_1'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Dato 1'))
      ->setDescription(t('Campo de tipo entero. Oblogatorio.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['dato_2'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Dato 2'))
      ->setDescription(t('Campo de tipo entero. Oblogatorio.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Etiqueta'))
      ->setDescription(t('Etiqueta.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of ejemplo_gubuy entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
