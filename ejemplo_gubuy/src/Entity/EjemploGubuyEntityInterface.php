<?php

namespace Drupal\ejemplo_gubuy\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining ejemplo_gubuy id entities.
 *
 * @ingroup ejemplo_gubuy
 */
interface EjemploGubuyEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets Ejemplo Gubuy dato_1.
   *
   * @return int
   *   Ejemplo Gubuy dato_1.
   */
  public function getDato1();

  /**
   * Sets the Ejemplo Gubuy dato_1.
   *
   * @param int $dato
   *   The dato_1.
   *
   * @return \Drupal\ejemplo_gubuy\Entity\EjemploGubuyEntityInterface
   *   The called ejemplo_gubuy id entity.
   */
  public function setDato1($dato);

  /**
   * Gets the Ejemplo Gubuy dato_2.
   *
   * @return int
   *   Ejemplo Gubuy dato_2.
   */
  public function getDato2();

  /**
   * Sets Ejemplo Gubuy dato_1.
   *
   * @param int $dato
   *   The data to set.
   *
   * @return \Drupal\ejemplo_gubuy\Entity\ejemplo_gubuyEntityInterface
   *   The called ejemplo_gubuy id entity.
   */
  public function setDato2($dato);

  /**
   * Gets the Ejemplo Gubuy creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ejemplo Gubuy entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Ejemplo Gubuy creation timestamp.
   *
   * @param int $timestamp
   *   The Ejemplo Gubuy creation timestamp.
   *
   * @return \Drupal\ejemplo_gubuy\Entity\ejemplo_gubuyEntityInterface
   *   The called ejemplo_gubuy id entity.
   */
  public function setCreatedTime($timestamp);

}
