<?php

namespace Drupal\ejemplo_gubuy\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for ejemplo_gubuy entities.
 */
class EjemploGubuyViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
