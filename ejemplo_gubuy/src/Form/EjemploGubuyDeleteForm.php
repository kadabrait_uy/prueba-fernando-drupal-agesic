<?php

namespace Drupal\ejemplo_gubuy\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ejemplo_gubuy entity entities.
 *
 * @ingroup ejemplo_gubuy
 */
class EjemploGubuyDeleteForm extends ContentEntityDeleteForm {


}
