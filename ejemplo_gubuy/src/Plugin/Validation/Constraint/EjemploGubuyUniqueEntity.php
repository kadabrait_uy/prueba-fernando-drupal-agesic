<?php

namespace Drupal\ejemplo_gubuy\Plugin\Validation\Constraint;

// Use Drupal\Core\StringTranslation\StringTranslationTrait;.
use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "EjemploGubuyUniqueEntity",
 *   label = @Translation("Unique Entity", context = "Validation"),
 *   type = "string"
 * )
 */
class EjemploGubuyUniqueEntity extends Constraint {

  /**
   * Message shown when the entity already exists.
   *
   * @var string
   */
  public $notUnique = 'An entity already exists with data 1 and data 2 values ​​entered.';

}
