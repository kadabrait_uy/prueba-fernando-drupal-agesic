<?php

namespace Drupal\ejemplo_gubuy\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class EjemploGubuyUniqueEntityValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Validator 2.5 and upwards compatible execution context.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EjemploGubuyUniqueEntityValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The user storage handler.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($item, Constraint $constraint) {
    $query_detail = $this->entityTypeManager->getStorage('ejemplo_gubuy')
      ->getQuery()
      ->condition('dato_1', $item->dato_1->value)
      ->condition('dato_2', $item->dato_2->value);
    $entity_ids = $query_detail->execute();
    if (count($entity_ids) > 0) {
      $this->context->addViolation($constraint->notUnique);
    }
  }

}
