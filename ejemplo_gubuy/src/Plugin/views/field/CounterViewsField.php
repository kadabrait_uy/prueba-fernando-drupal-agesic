<?php

namespace Drupal\ejemplo_gubuy\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field result for view ejercicio_gubuy.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ejemplo_gubuy_resultado")
 */
class CounterViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['counter_start'] = ['default' => 1];
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['resultado'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Results'),
      '#default_value' => '',
      '#description' => $this->t('Calculate result using dato_1 and dato_1 fields.'),
      '#size' => 2,
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_entity;
    $result = ($entity->dato_1->value * 5) + ($entity->dato_2->value * 10);
    return $result;
  }

}
